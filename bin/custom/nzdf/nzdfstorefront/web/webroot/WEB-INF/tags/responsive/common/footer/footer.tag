<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer>
	<img src="${commonResourcePath}/images/nzdf_socialmedia_iconbanner2.jpg">
	<img class="joinUs" src="${commonResourcePath}/images/btn-joinus.jpg">
    <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <img src="${commonResourcePath}/images/f4nz.png">
	<img class="joinUs" src="${commonResourcePath}/images/logo-new-zealand-government.png">
</footer>

