/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jul 4, 2018 1:04:46 PM                      ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.nzdf.fulfilmentprocess.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedNzdfFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "nzdffulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedNzdfFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
