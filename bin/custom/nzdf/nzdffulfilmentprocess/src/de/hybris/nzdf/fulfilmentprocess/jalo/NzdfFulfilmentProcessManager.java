/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.nzdf.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.nzdf.fulfilmentprocess.constants.NzdfFulfilmentProcessConstants;

public class NzdfFulfilmentProcessManager extends GeneratedNzdfFulfilmentProcessManager
{
	public static final NzdfFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (NzdfFulfilmentProcessManager) em.getExtension(NzdfFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
