/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.nzdf.initialdata.constants;

/**
 * Global class for all NzdfInitialData constants.
 */
public final class NzdfInitialDataConstants extends GeneratedNzdfInitialDataConstants
{
	public static final String EXTENSIONNAME = "nzdfinitialdata";

	private NzdfInitialDataConstants()
	{
		//empty
	}
}
